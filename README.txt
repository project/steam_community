
README
---------------------------
A module that integrates Valve Software's Steam Community features with your site.  Currently, this includes:
- Display a block containing the latest events for a particular Steam Community group


Installation
---------------------------
1. Install the module through the standard Drupal 5.x workflow
2. Configure the module at administer -> settings -> steam community (Information on settings located there)
3. Set permissions for access the module data.  Two new permissions are provided:
   - access steam community pages
   - configure steam community pages
4. Enable the Steam Community block on your site.


Notes
---------------------------
- Cron must be running regularly to wipe out the cached events on the requested schedule.


Authors
---------------------------
Jeff Beeman (jeffbeeman.com)